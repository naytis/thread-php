export const PUSH_COMMENTS = 'pushComments';
export const ADD_COMMENT = 'addComment';
export const SET_COMMENT_IMAGE = 'setCommentImage';
export const SET_COMMENT = 'editComment';
export const DELETE_COMMENT = 'deleteComment';
export const LIKE_COMMENT = 'likeComment';
export const DISLIKE_COMMENT = 'dislikeComment';
