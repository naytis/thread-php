<?php

declare(strict_types=1);

namespace App\Http\Controllers\Api;

use App\Action\Comment\AddCommentAction;
use App\Action\Comment\AddCommentRequest;
use App\Action\Comment\DeleteCommentAction;
use App\Action\Comment\DeleteCommentRequest;
use App\Action\Comment\GetCommentByIdAction;
use App\Action\Comment\GetCommentCollectionAction;
use App\Action\Comment\GetCommentCollectionByTweetIdAction;
use App\Action\Comment\GetUsersWhoLikedCollectionByCommentIdAction;
use App\Action\Comment\UpdateCommentAction;
use App\Action\Comment\UpdateCommentRequest;
use App\Action\Comment\UploadCommentImageAction;
use App\Action\Comment\UploadCommentImageRequest;
use App\Action\GetByIdRequest;
use App\Action\GetCollectionRequest;
use App\Http\Controllers\ApiController;
use App\Http\Presenter\CommentAsArrayPresenter;
use App\Http\Presenter\UserArrayPresenter;
use App\Http\Request\Api\AddCommentHttpRequest;
use App\Http\Request\Api\Comment\UpdateCommentHttpRequest;
use App\Http\Request\Api\Comment\UploadCommentImageHttpRequest;
use App\Http\Response\ApiResponse;
use App\Http\Request\Api\CollectionHttpRequest;
use App\Action\Comment\GetCommentCollectionByTweetIdRequest;
use App\Action\Comment\GetUsersWhoLikedCollectionByCommentIdRequest;

final class CommentController extends ApiController
{
    private $getCommentCollectionAction;
    private $commentAsArrayPresenter;
    private $userArrayPresenter;
    private $getCommentByIdAction;
    private $addCommentAction;
    private $getCommentCollectionByTweetIdAction;
    private $updateCommentAction;
    private $deleteCommentAction;
    private $uploadCommentImageAction;
    private $getUsersWhoLikedCollectionByCommentIdAction;

    public function __construct(
        GetCommentCollectionAction $getCommentCollectionAction,
        CommentAsArrayPresenter $commentAsArrayPresenter,
        UserArrayPresenter $userArrayPresenter,
        GetCommentByIdAction $commentByIdAction,
        AddCommentAction $addCommentAction,
        GetCommentCollectionByTweetIdAction $getCommentCollectionByTweetIdAction,
        UpdateCommentAction $updateCommentAction,
        DeleteCommentAction $deleteCommentAction,
        UploadCommentImageAction $uploadCommentImageAction,
        GetUsersWhoLikedCollectionByCommentIdAction $getUsersWhoLikedCollectionByCommentIdAction
    ) {
        $this->getCommentCollectionAction = $getCommentCollectionAction;
        $this->commentAsArrayPresenter = $commentAsArrayPresenter;
        $this->userArrayPresenter = $userArrayPresenter;
        $this->getCommentByIdAction = $commentByIdAction;
        $this->addCommentAction = $addCommentAction;
        $this->getCommentCollectionByTweetIdAction = $getCommentCollectionByTweetIdAction;
        $this->updateCommentAction = $updateCommentAction;
        $this->deleteCommentAction = $deleteCommentAction;
        $this->uploadCommentImageAction = $uploadCommentImageAction;
        $this->getUsersWhoLikedCollectionByCommentIdAction = $getUsersWhoLikedCollectionByCommentIdAction;
    }

    public function getCommentCollection(CollectionHttpRequest $request): ApiResponse
    {
        $response = $this->getCommentCollectionAction->execute(
            new GetCollectionRequest(
                (int)$request->query('page'),
                $request->query('sort'),
                $request->query('direction')
            )
        );

        return $this->createPaginatedResponse($response->getPaginator(), $this->commentAsArrayPresenter);
    }

    public function getCommentById(string $id): ApiResponse
    {
        $comment = $this->getCommentByIdAction->execute(new GetByIdRequest((int)$id))->getComment();

        return $this->createSuccessResponse($this->commentAsArrayPresenter->present($comment));
    }

    public function newComment(AddCommentHttpRequest $request): ApiResponse
    {
        $response = $this->addCommentAction->execute(
            new AddCommentRequest(
                $request->get('body'),
                (int)$request->get('tweet_id')
            )
        );

        return $this->created(
            $this->commentAsArrayPresenter->present(
                $response->getComment()
            )
        );
    }

    public function getCommentCollectionByTweetId(string $tweetId, CollectionHttpRequest $request): ApiResponse
    {
        $response = $this->getCommentCollectionByTweetIdAction->execute(
            new GetCommentCollectionByTweetIdRequest(
                (int)$tweetId,
                (int)$request->query('page'),
                $request->query('sort'),
                $request->query('direction')
            )
        );

        return $this->createPaginatedResponse($response->getPaginator(), $this->commentAsArrayPresenter);
    }

    public function updateCommentById(string $id, UpdateCommentHttpRequest $request): ApiResponse
    {
        $response = $this->updateCommentAction->execute(
            new UpdateCommentRequest(
                (int)$id,
                $request->get('body')
            )
        );

        return $this->createSuccessResponse(
            $this->commentAsArrayPresenter->present(
                $response->getComment()
            )
        );
    }

    public function deleteCommentById(string $id): ApiResponse
    {
        $this->deleteCommentAction->execute(new DeleteCommentRequest((int)$id));

        return $this->createDeletedResponse();
    }

    public function uploadCommentImage(string $id, UploadCommentImageHttpRequest $request): ApiResponse
    {
        $response = $this->uploadCommentImageAction->execute(
            new UploadCommentImageRequest(
                (int)$id,
                $request->file('image')
            )
        );

        return $this->createSuccessResponse(
            $this->commentAsArrayPresenter->present(
                $response->getComment()
            )
        );
    }

    public function getUsersWhoLikedCollectionByCommentId(string $commentId, CollectionHttpRequest $request): ApiResponse
    {
        $response = $this->getUsersWhoLikedCollectionByCommentIdAction->execute(
            new GetUsersWhoLikedCollectionByCommentIdRequest(
                (int)$commentId,
                (int)$request->query('page'),
                $request->query('sort'),
                $request->query('direction')
            )
        );

        return $this->createPaginatedResponse($response->getPaginator(), $this->userArrayPresenter);
    }
}
