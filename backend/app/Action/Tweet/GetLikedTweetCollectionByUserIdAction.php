<?php

declare(strict_types=1);

namespace App\Action\Tweet;

use App\Action\PaginatedResponse;
use App\Repository\LikeRepository;

final class GetLikedTweetCollectionByUserIdAction
{
    private $likeRepository;

    public function __construct(LikeRepository $likeRepository)
    {
        $this->likeRepository = $likeRepository;
    }

    public function execute(GetLikedTweetCollectionByUserIdRequest $request): PaginatedResponse
    {
        return new PaginatedResponse(
            $this->likeRepository->getPaginatedByUserId(
                $request->getUserId(),
                $request->getPage() ?: LikeRepository::DEFAULT_PAGE,
                LikeRepository::DEFAULT_PER_PAGE,
                $request->getSort() ?: LikeRepository::DEFAULT_SORT,
                $request->getDirection() ?: LikeRepository::DEFAULT_DIRECTION
            )
        );
    }
}
