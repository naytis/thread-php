<?php

declare(strict_types=1);

namespace App\Action\Tweet;

use App\Action\PaginatedResponse;
use App\Repository\TweetRepository;

final class GetUsersWhoLikedCollectionByTweetIdAction
{
    private $tweetRepository;

    public function __construct(TweetRepository $tweetRepository)
    {
        $this->tweetRepository = $tweetRepository;
    }

    public function execute(GetUsersWhoLikedCollectionByTweetIdRequest $request): PaginatedResponse
    {
        return new PaginatedResponse(
            $this->tweetRepository->getPaginatedByTweetId(
                $request->getTweetId(),
                $request->getPage() ?: TweetRepository::DEFAULT_PAGE,
                60,
                $request->getSort() ?: TweetRepository::DEFAULT_SORT,
                $request->getDirection() ?: TweetRepository::DEFAULT_DIRECTION
            )
        );
    }
}
