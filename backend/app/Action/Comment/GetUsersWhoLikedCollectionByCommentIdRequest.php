<?php

declare(strict_types=1);

namespace App\Action\Comment;

use App\Action\GetCollectionRequest;

final class GetUsersWhoLikedCollectionByCommentIdRequest extends GetCollectionRequest
{
    private $commentId;

    public function __construct(int $commentId, ?int $page, ?string $sort, ?string $direction)
    {
        parent::__construct($page, $sort, $direction);

        $this->commentId = $commentId;
    }

    public function getCommentId(): int
    {
        return $this->commentId;
    }
}
