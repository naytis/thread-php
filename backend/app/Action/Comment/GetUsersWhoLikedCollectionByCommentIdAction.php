<?php

declare(strict_types=1);

namespace App\Action\Comment;

use App\Action\PaginatedResponse;
use App\Repository\CommentRepository;

final class GetUsersWhoLikedCollectionByCommentIdAction
{
    private $commentRepository;

    public function __construct(CommentRepository $commentRepository)
    {
        $this->commentRepository = $commentRepository;
    }

    public function execute(GetUsersWhoLikedCollectionByCommentIdRequest $request): PaginatedResponse
    {
        return new PaginatedResponse(
            $this->commentRepository->getPaginatedByCommentId(
                $request->getCommentId(),
                $request->getPage() ?: CommentRepository::DEFAULT_PAGE,
                60,
                $request->getSort() ?: CommentRepository::DEFAULT_SORT,
                $request->getDirection() ?: CommentRepository::DEFAULT_DIRECTION
            )
        );
    }
}
