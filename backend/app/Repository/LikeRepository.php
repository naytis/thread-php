<?php

declare(strict_types=1);

namespace App\Repository;

use App\Entity\Comment;
use App\Entity\Like;
use App\Entity\Tweet;
use Illuminate\Contracts\Pagination\LengthAwarePaginator;

final class LikeRepository implements Paginable
{
    public function save(Like $like): Like
    {
        $like->save();

        return $like;
    }

    public function paginate(
        int $page = self::DEFAULT_PAGE,
        int $perPage = self::DEFAULT_PER_PAGE,
        string $sort = self::DEFAULT_SORT,
        string $direction = self::DEFAULT_DIRECTION
    ): LengthAwarePaginator {
        return Like::orderBy($sort, $direction)->paginate($perPage, ['*'], null, $page);
    }

    public function existsForTweetByUser(int $tweetId, int $userId): bool
    {
        return Like::where([
            'likeable_id' => $tweetId,
            'likeable_type' => Tweet::class,
            'user_id' => $userId
        ])->exists();
    }

    public function getPaginatedByUserId(
        int $userId,
        int $page = self::DEFAULT_PAGE,
        int $perPage = self::DEFAULT_PER_PAGE,
        string $sort = self::DEFAULT_SORT,
        string $direction = self::DEFAULT_DIRECTION
    ): LengthAwarePaginator {
        $likedTweetsIds = Like::where([
                'user_id'=> $userId,
                'likeable_type' => Tweet::class
            ])
            ->pluck('likeable_id');

        return Tweet::whereIn('id', $likedTweetsIds)
            ->orderBy($sort, $direction)
            ->paginate($perPage, ['*'], null, $page);
    }

    public function deleteForTweetByUser(int $tweetId, int $userId): void
    {
        Like::where([
            'likeable_id' => $tweetId,
            'likeable_type' => Tweet::class,
            'user_id' => $userId
        ])->delete();
    }

    public function existsForCommentByUser(int $commentId, int $userId): bool
    {
        return Like::where([
            'likeable_id' => $commentId,
            'likeable_type' => Comment::class,
            'user_id' => $userId
        ])->exists();
    }

    public function deleteForCommentByUser(int $commentId, int $userId): void
    {
        Like::where([
            'likeable_id' => $commentId,
            'likeable_type' => Comment::class,
            'user_id' => $userId
        ])->delete();
    }
}
